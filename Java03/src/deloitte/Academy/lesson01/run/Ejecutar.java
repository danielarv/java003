package deloitte.Academy.lesson01.run;

import java.util.logging.Logger;
import deloitte.Academy.lesson01.operation.Calzado;
import deloitte.Academy.lesson01.operation.Electronica;
import deloitte.Academy.lesson01.operation.LineaBlanca;
import deloitte.Academy.lesson01.operation.Ropa;

/**
 * Esta clase imprime todos los productos disponibles por categorias Imprime la
 * cantidad de productos vendidos por categoria y en almacen Busca productos
 * dentro de las categorias Agrega productos y los agrega a almacen
 * 
 * @author dareyes
 *
 */
public class Ejecutar {

	public static void main(String[] args) {

		// Imprimir productos por categoria
		System.out.println("--------PRODUCTOS POR CATEGORIA------- \n");

		System.out.println("Productos del Depto. de Electronica: " + Electronica.ELECTRONICA.getCelulares() + ", "
				+ Electronica.ELECTRONICA.getComputadoras() + ", " + Electronica.ELECTRONICA.getBocinas());
		System.out.println("Productos del Depto. de Linea Blanca: " + LineaBlanca.LINEABLANCA.getLavadoras() + ", "
				+ LineaBlanca.LINEABLANCA.getEstufas() + ", " + LineaBlanca.LINEABLANCA.getRefrigeradores());
		System.out.println("Productos del Depto. de Ropa: " + Ropa.ROPA.getAbrigos() + ", " + Ropa.ROPA.getPantalones()
				+ ", " + Ropa.ROPA.getCamisas());
		System.out.println(
				"Productos del Depto. de Calzado: " + Calzado.CALZADO.getBotas() + ", " + Calzado.CALZADO.getZapatos());

		System.out.println("\n");

		// Imprimir numero de productos por categoria VENDIDOS
		System.out.println("--------PRODUCTOS VENDIDOS POR CATEGORIA------- \n");

		System.out.println("Productos vendidos del Depto. de Electronica: " + Electronica.ELECTRONICA.getVendidos());
		System.out.println("Productos vendidos del Depto. de Linea Blanca: " + LineaBlanca.LINEABLANCA.getVendidos());
		System.out.println("Productos vendidos del Depto. de Ropa: " + Ropa.ROPA.getVendidos());
		System.out.println("Productos vendidos del Depto. de Calzado: " + Calzado.CALZADO.getVendidos());

		// Imprimir numero de productos por categoria en ALMACEN
		System.out.println("\n");
		System.out.println("--------PRODUCTOS EN STOCK POR CATEGORIA------- \n");

		System.out.println("Productos almacenados en STOCK del Depto. de Electronica: "
				+ Electronica.ELECTRONICA.getAlmacenados());
		System.out.println("Productos almacenados en STOCK del Depto. de Linea Blanca: "
				+ LineaBlanca.LINEABLANCA.getAlmacenados());
		System.out.println("Productos almacenados en STOCK del Depto. de Ropa: " + Ropa.ROPA.getAlmacenados());
		System.out.println("Productos almacenados en STOCK del Depto. de Calzado: " + Calzado.CALZADO.getAlmacenados());

		System.out.println("\n");

		System.out.println("---------PRODUCTOS AGREGADOS AL ALMACEN---------");

		// Registro de nuevo producto y agregarlo al stock

		// Agregar producto a la lista de electronica

		Electronica.ELECTRONICA.setNuevoProducto("Televisiones");

		System.out.print("Se agrego un nuevo producto a la categoria de Electronica: ");

		// Obtener el numero de productos almacenados en electronica (en general) y
		// agregarle la cantidad que se agrego
		int Almacenados = Electronica.ELECTRONICA.getAlmacenados();
		int newStock = Almacenados + 1;
		System.out.print("Ahora el stock de Electronica tiene " + newStock + " productos.");

		System.out.println("\n");
		
		System.out.println("---------BUSQUEDA DE PRODUCTOS---------");

		// Buscar producto por categoria
		String clave = Ejecutar.buscarPalabra();
		System.out.println("La palabra que buscaste es: " + clave);

	}

	// buscar un producto en especifico dentro de una categoria
	public static String buscarPalabra() {
			
			final Logger LOGGER = Logger.getLogger(Ejecutar.class.getCanonicalName());
			
			  // Busqueda de una palabra en la lista de Linea Blanca
			

			String[] productosDeElectronica = { "Celulares", "Bocinas", "Computadoras"};
			
			String[] productosDeLineaBlanca = { "Estufas", "Lavadoras", "Refrigeradores"};
			
			String[] productosDeRopa = { "Abrigos", "Pantalones", "Camisas"};
			
			String[] productosDeCalzado = {"Botas", "Zapatos"};
					
		//PALABRA QUE SE ESTA BUSCANDO
		String clave = "Alimentos";
		try {
			boolean bandera= false;
			for (String elemento1 : productosDeElectronica) {
				if (elemento1.equals(clave)) {
					bandera=true;
					System.out.println("Tu producto " + clave + "se encuentra en la categoria de Electronica"); break;
				} 
			}
			for (String elemento2 : productosDeLineaBlanca) {
				if (elemento2.equals(clave)) {
					bandera=true;
					System.out.println("Tu producto " + clave + "se encuentra en la categoria de Linea Blanca"); break;
						} 
			}
		    for (String elemento3 : productosDeRopa) {
				if (elemento3.equals(clave)) {
					bandera=true;
					System.out.println("Tu producto " + clave + "se encuentra en la categoria de Ropa"); break;
								} 
		    }
			for (String elemento4 : productosDeCalzado) {
				if (elemento4.equals(clave)) {
					bandera=true;
					System.out.println("Tu producto " + clave + "se encuentra en la categoria de Calzado"); break;
								}
			}
			if(!bandera) {
				System.out.println("No se encontr� el producto en el almac�n");
			}
					

	}catch(Exception e) {
			LOGGER.info("Tu producto no se encuentra en almacen.");
		}return clave;
}

}
