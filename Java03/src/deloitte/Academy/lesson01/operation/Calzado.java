package deloitte.Academy.lesson01.operation;

public enum Calzado {

	//Atributos que tendra la categoria de electronica
		CALZADO(80,56, "botas", "zapatos");
		
		//Constructores
		Calzado(int vendidos, int almacenados, String botas, String zapatos) {
			this.vendidos = vendidos;
			this.almacenados = almacenados;
			this.botas = botas;
			this.zapatos = zapatos;
		}

		private Calzado() {
			
		}
		
		//Atributos de la clase Electronica (getters and setters)
		
		public int vendidos;
		public int almacenados;
		public String botas;
		public String zapatos;
		
		public int getVendidos() {
			return vendidos;
		}
		public void setVendidos(int vendidos) {
			this.vendidos = vendidos;	
		}
		
		
		public int getAlmacenados() {
			return almacenados;
		}
		public void setAlmacenados(int almacenados) {
			this.almacenados = almacenados;
		}

		public String getBotas() {
			return botas;
		}

		public void setBotas(String botas) {
			this.botas = botas;
		}

		public String getZapatos() {
			return zapatos;
		}

		public void setZapatos(String zapatos) {
			this.zapatos = zapatos;
		}
		
		
	}

