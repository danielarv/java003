package deloitte.Academy.lesson01.operation;

public enum Ropa {
	
	//Atributos que tendra la categoria de electronica
		ROPA(180,250, "abrigos", "pantalones", "camisas");
		
		//Constructores
		Ropa(int vendidos, int almacenados, String abrigos, String pantalones, String camisas) {
			this.vendidos = vendidos;
			this.almacenados = almacenados;
			this.abrigos = abrigos;
			this.pantalones = pantalones;
			this.camisas = camisas;
		}

		private Ropa() {
			
		}
		
		//Atributos de la clase Electronica (getters and setters)
		
		public int vendidos;
		public int almacenados;
		public String abrigos;
		public String pantalones;
		public String camisas;
		
		public int getVendidos() {
			return vendidos;
		}
		public void setVendidos(int vendidos) {
			this.vendidos = vendidos;	
		}
		
		
		public int getAlmacenados() {
			return almacenados;
		}
		public void setAlmacenados(int almacenados) {
			this.almacenados = almacenados;
		}

		public String getAbrigos() {
			return abrigos;
		}

		public void setAbrigos(String abrigos) {
			this.abrigos = abrigos;
		}

		public String getPantalones() {
			return pantalones;
		}

		public void setPantalones(String pantalones) {
			this.pantalones = pantalones;
		}

		public String getCamisas() {
			return camisas;
		}

		public void setCamisas(String camisas) {
			this.camisas = camisas;
		}
		
		
	

}
