package deloitte.Academy.lesson01.operation;

public enum LineaBlanca {

	//Atributos que tendra la categoria de electronica
		LINEABLANCA(12,24, "lavadoras", "estufas", "refrigeradores");
		
		//Constructores
		LineaBlanca(int vendidos, int almacenados, String lavadoras, String estufas, String refrigeradores) {
			this.vendidos = vendidos;
			this.almacenados = almacenados;
			this.lavadoras = lavadoras;
			this.estufas = estufas;
			this.refrigeradores = refrigeradores;
		}

		private LineaBlanca() {
			
		}
		
		//Atributos de la clase Electronica (getters and setters)
		
		public int vendidos;
		public int almacenados;
		public String lavadoras;
		public String estufas;
		public String refrigeradores;
		
		public int getVendidos() {
			return vendidos;
		}
		
		public void setVendidos(int vendidos) {
			this.vendidos = vendidos;	
		}
		
		public int getAlmacenados() {
			return almacenados;
		}
		
		public void setAlmacenados(int almacenados) {
			this.almacenados = almacenados;
		}

		public String getLavadoras() {
			return lavadoras;
		}

		public void setLavadoras(String lavadoras) {
			this.lavadoras = lavadoras;
		}

		public String getEstufas() {
			return estufas;
		}

		public void setEstufas(String estufas) {
			this.estufas = estufas;
		}

		public String getRefrigeradores() {
			return refrigeradores;
		}

		public void setRefrigeradores(String refrigeradores) {
			this.refrigeradores = refrigeradores;
		}
		
		
		
	}

