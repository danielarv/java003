package deloitte.Academy.lesson01.operation;

public enum Electronica {

	// Atributos que tendra la categoria de electronica
	ELECTRONICA(30, 45, "celulares", "computadoras", "bocinas", " ");

	// Constructores
	Electronica(int vendidos, int almacenados, String celulares, String computadoras, String bocinas, String nuevoProducto) {
		this.vendidos = vendidos;
		this.almacenados = almacenados;
		this.celulares = celulares;
		this.computadoras = computadoras;
		this.bocinas = bocinas;
		this.nuevoProducto = nuevoProducto;

	}

	private Electronica() {

	}

	// Atributos de la clase Electronica (getters and setters)

	public int vendidos;
	public int almacenados;
	public String celulares;
	public String computadoras;
	public String bocinas;
	public String nuevoProducto;

	public int getVendidos() {
		return vendidos;
	}

	public void setVendidos(int vendidos) {
		this.vendidos = vendidos;
	}

	public int getAlmacenados() {
		return almacenados;
	}

	public void setAlmacenados(int almacenados) {
		this.almacenados = almacenados;
	}

	public String getCelulares() {
		return celulares;
	}

	public void setCelulares(String celulares) {
		this.celulares = celulares;
	}

	public String getComputadoras() {
		return computadoras;
	}

	public void setComputadoras(String computadoras) {
		this.computadoras = computadoras;
	}

	public String getBocinas() {
		return bocinas;
	}

	public void setBocinas(String bocinas) {
		this.bocinas = bocinas;
	}

	public String getNuevoProducto(String nuevoProducto) {
		return nuevoProducto;
	}

	public void setNuevoProducto(String nuevoProducto) {
		this.nuevoProducto = nuevoProducto;
	}

}
